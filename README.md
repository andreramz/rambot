## RAMBOT
---

Simple Youtube notifier Discord bot with the support of REST API.

Developed by @andreramz. **ONLY SUPPORT LINUX ENVIRONMENT**.

### Prerequisites
- Ruby 2.6.6 (prefer using RVM)

### How to Run

1. Install dependencies

```
gem install bundler
bundle install
```

2. Run application
```
bundle exec puma -C config/puma.rb config.ru
```

### Bot Commands

TODO
