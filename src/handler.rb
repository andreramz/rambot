class Handler < Sinatra::Base
  get '/' do
    'Hello there!'
  end

  # Feed A: Andi Adinata
  get '/feed-a' do
    check_callbacks
  end

  post '/feed-a' do
    notify(:response_1)
  end

  # Feed B: Alia Adelia
  get '/feed-b' do
    check_callbacks
  end

  post '/feed-b' do
    notify(:response_2)
  end

  # Feed C: Lumi Celestia
  get '/feed-c' do
    check_callbacks
  end

  post '/feed-c' do
    notify(:response_3)
  end

  # Feed D: Zen Gunawan
  get '/feed-d' do
    check_callbacks
  end

  post '/feed-d' do
    notify(:response_4)
  end

  # Feed E: Nia Redalion
  get '/feed-e' do
    check_callbacks
  end

  post '/feed-e' do
    notify(:response_5)
  end

  private

  # Youtube will check for callbacks, mostly for diagnostic purposes.
  def check_callbacks
    status 200
    params["hub.challenge"]
  end

  def notify(response_code)
    feed = Feedjira.parse(request.body.read.clone)
    link_str = feed.entries.first.url

    link_cache = $cache.get("link:#{link_str}")

    if link_cache.nil?
      CHANNEL_ID.each do |ch_id|
        $bot.send_message(ch_id, Response.send(response_code, link_str))
      end

      $cache.set("link:#{link_str}", link_str, expires_in: CACHE_EXPIRE_TIME_SECONDS)
    end

    status 200
    'ok'
  end
end
