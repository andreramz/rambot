class Response
  class << self
    def response_1(link)
      <<-STR
      Halo @everyone, Andi baru aja upload video baru.

      Link: #{link}
      STR
    end

    def response_2(link)
      <<-STR
      Halo @everyone, Alia baru aja upload video baru.

      Link: #{link}
      STR
    end

    def response_3(link)
      <<-STR
      Halo @everyone, Lumi baru aja upload video baru.

      Link: #{link}
      STR
    end

    def response_4(link)
      <<-STR
      Halo @everyone, Zen baru aja upload video baru.

      Link: #{link}
      STR
    end

    def response_5(link)
      <<-STR
      Halo @everyone, Nia baru aja upload video baru.

      Link: #{link}
      STR
    end
  end
end
