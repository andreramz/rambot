require 'discordrb'
require './requirer.rb'

# Constants
BOT_TOKEN = ENV['BOT_TOKEN']
CHANNEL_ID = ENV['CHANNEL_ID'].split(',')
CACHE_EXPIRE_TIME_SECONDS = 43200

# Instantiate a bot as global variable
$bot = Discordrb::Bot.new token: BOT_TOKEN
require './lib_requirer.rb'

# Initialize in-memory cache
$cache = MiniCache::Store.new

puts 'Hello world!'

# handle at exit
at_exit {
  $bot.stop
}

# run bot in background
$bot.run(true)
