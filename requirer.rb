require 'bundler'
require 'dotenv'
require 'feedjira'
require 'httparty'
require 'newrelic_rpm'
require 'pry'
require 'require_all'
require 'sinatra'

# require bundle
Bundler.require

# load env
Dotenv.load

configure do
  set :server, :puma
end
